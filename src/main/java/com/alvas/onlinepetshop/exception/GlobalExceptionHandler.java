package com.alvas.onlinepetshop.exception;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
	

		@Override
		protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {

			List<String> details = new ArrayList<>();
			for (ObjectError error : ex.getBindingResult().getAllErrors()) {
				details.add(error.getDefaultMessage());
			}

			ErrorResponse errorResponse = new ErrorResponse(4000L, details);
			return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
		}

		
		@ExceptionHandler(value = InvalidUserNameAndPasswordException.class)
		public ResponseEntity<Object> accountNotFoundException(InvalidUserNameAndPasswordException exception, WebRequest req) {
			List<String> details = new ArrayList<>();
			details.add(exception.getLocalizedMessage());

			ErrorResponse errorResponse = new ErrorResponse(5000L, details);
			return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

		}

		@ExceptionHandler(value =SQLIntegrityConstraintViolationException.class)
		public ResponseEntity<Object> sqlIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException exception, WebRequest req) {
			List<String> details = new ArrayList<>();
			details.add(exception.getLocalizedMessage());

			ErrorResponse errorResponse = new ErrorResponse(5000L, details);
			return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

		
	}
}



