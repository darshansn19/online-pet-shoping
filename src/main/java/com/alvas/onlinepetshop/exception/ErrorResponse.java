package com.alvas.onlinepetshop.exception;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {

		private long errorCode;
		private List<String> message;

		public ErrorResponse(long errorCode, List<String> message) {
			super();
			this.errorCode = errorCode;
			this.message = message;
		}

	}


