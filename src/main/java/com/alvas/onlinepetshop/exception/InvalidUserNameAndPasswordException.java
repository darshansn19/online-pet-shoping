package com.alvas.onlinepetshop.exception;

public class InvalidUserNameAndPasswordException  extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUserNameAndPasswordException(String message) {
		super(message);

	}

}
