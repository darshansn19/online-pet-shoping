package com.alvas.onlinepetshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.onlinepetshop.dto.PurchasePet;
import com.alvas.onlinepetshop.serviceimpl.PurchasePetServiceImpl;

@RestController
@RequestMapping(value = "pet")
public class PetPurchaseHistoryController {

	@Autowired
	PurchasePetServiceImpl impl;

	@GetMapping("/history")
	public List<PurchasePet> purchaseHistory(@RequestParam int userId) {
		return impl.getPurchaseHistory(userId);
	}

}
