package com.alvas.onlinepetshop.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.responseDto.PetDto;
import com.alvas.onlinepetshop.serviceimpl.PetadminServiceImpl;

@RestController
@RequestMapping(value = "pet")
public class PetAdminController {

	@Autowired
	PetadminServiceImpl serviceImpl;

	@PostMapping(value = "/add")
	public Response addPet(@Valid @RequestBody PetDto petdetails) {
		Pet pet = new Pet();
		pet.setPetId(petdetails.getPetId());
		pet.setPetName(petdetails.getPetName());
		pet.setQuantity(petdetails.getQuantity());

		return serviceImpl.addPet(pet);
	}
}
