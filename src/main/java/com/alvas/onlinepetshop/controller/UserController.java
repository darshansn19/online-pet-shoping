package com.alvas.onlinepetshop.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.dto.User;
import com.alvas.onlinepetshop.responseDto.UserDto;
import com.alvas.onlinepetshop.serviceimpl.UserServiceImpl;

@RestController
@RequestMapping(value = "login")
public class UserController {

	@Autowired
	UserServiceImpl serviceImpl;

	@PostMapping(value = "/")
	public ResponseEntity<Response> userLogin(@Valid @RequestBody UserDto dto) {
		User user = new User();
		user.setUserId(dto.getUserId());
		user.setUserName(dto.getUserName());
		user.setPassword(dto.getPassword());
		user.setEmail(dto.getEmail());
		user.setStatus("no");

		return new ResponseEntity<>(serviceImpl.userLogin(user), HttpStatus.OK);

	}

}
