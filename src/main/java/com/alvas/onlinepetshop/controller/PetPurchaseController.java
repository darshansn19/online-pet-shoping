package com.alvas.onlinepetshop.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.onlinepetshop.dto.PurchasePet;
import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.responseDto.PurchasePetDto;
import com.alvas.onlinepetshop.serviceimpl.PurchasePetServiceImpl;

@RestController
@RequestMapping(value = "pet")
public class PetPurchaseController {

	@Autowired
	PurchasePetServiceImpl purchasePetServiceImpl;

	@PostMapping(value = "/purchase")
	public ResponseEntity<Response> PurchasePets(@Valid @RequestBody PurchasePetDto purchasePetDto) {
		PurchasePet purchasePet = new PurchasePet();
		purchasePet.setPetSoldId(purchasePetDto.getPetSoldId());
		purchasePet.setUserId(purchasePetDto.getUserId());
		purchasePet.setPetId(purchasePetDto.getPetId());
		purchasePet.setDate(purchasePetDto.getDate());
		purchasePet.setQuantity(purchasePetDto.getQuantity());
		return new ResponseEntity<>(purchasePetServiceImpl.purchasePets(purchasePet), HttpStatus.OK);

	}
}
