package com.alvas.onlinepetshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.serviceimpl.PetServiceImpl;

@RestController
@RequestMapping(value = "pet")
public class PetController {

	@Autowired
	PetServiceImpl petServiceImpl;

	@GetMapping("/search")
	public List<Pet> getAllPets() {
		return petServiceImpl.getAllPets();
	}

}
