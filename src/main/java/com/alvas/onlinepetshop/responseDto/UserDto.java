package com.alvas.onlinepetshop.responseDto;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;

@Data
public class UserDto {

	private int userId;

	@NotEmpty(message = "provide user name")
	private String userName;

	@NotEmpty(message = "provide password")
	private String password;

	@NotEmpty(message = "Email is required")
	@Email(message = "enter valid  email format", regexp = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$")
	@Column(unique = true)
	private String email;

	@ColumnDefault(value = "null")
	private String status;

}
