package com.alvas.onlinepetshop.responseDto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class PetDto {

	private int petId;
	
	@NotEmpty(message = "give pet name")
	private String petName;
	
	@Min(value = 0,message = "give petquantity")
	private int quantity;

}
