package com.alvas.onlinepetshop.responseDto;

import java.time.LocalDate;

import javax.validation.constraints.Min;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Data
public class PurchasePetDto {
	
	private int petSoldId;

	@Min(value = 0,message  = "user Id required")
	private int userId;

	@Min(value = 0,message  = "pet Id required")
	private int petId;

	@CreationTimestamp
	private LocalDate date;

	@Min(1)
	private int quantity;

}
