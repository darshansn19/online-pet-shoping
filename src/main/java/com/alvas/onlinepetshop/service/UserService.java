package com.alvas.onlinepetshop.service;

import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.dto.User;

public interface UserService {
	 Response userLogin(User user);
}
