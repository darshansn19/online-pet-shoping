package com.alvas.onlinepetshop.service;

import java.util.List;

import com.alvas.onlinepetshop.dto.PurchasePet;
import com.alvas.onlinepetshop.dto.Response;

public interface PurchasePetService {
	Response purchasePets(PurchasePet pet);

	List<PurchasePet> getPurchaseHistory(int id);
}
