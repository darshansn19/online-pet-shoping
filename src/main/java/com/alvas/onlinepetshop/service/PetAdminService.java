package com.alvas.onlinepetshop.service;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.dto.Response;

public interface PetAdminService {
	Response  addPet(Pet pet);

}
