package com.alvas.onlinepetshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alvas.onlinepetshop.dto.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Integer> {

	 //List<Pet> getPetByPetNamecontains(String name);
}
