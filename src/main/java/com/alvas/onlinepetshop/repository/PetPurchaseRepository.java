package com.alvas.onlinepetshop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alvas.onlinepetshop.dto.PurchasePet;

@Repository
public interface PetPurchaseRepository extends JpaRepository<PurchasePet, Integer>{

	List<PurchasePet> findByUserId(int id);
	
	List<PurchasePet> getPurchaseHistoryByUserId(int id);
	
	

}
