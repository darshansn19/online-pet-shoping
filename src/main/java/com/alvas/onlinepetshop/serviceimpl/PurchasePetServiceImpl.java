package com.alvas.onlinepetshop.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.dto.PurchasePet;
import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.dto.User;
import com.alvas.onlinepetshop.repository.PetPurchaseRepository;
import com.alvas.onlinepetshop.repository.PetRepository;
import com.alvas.onlinepetshop.repository.UserRepository;
import com.alvas.onlinepetshop.service.PurchasePetService;

@Service
public class PurchasePetServiceImpl implements PurchasePetService {

	@Autowired
	PetPurchaseRepository petPurchaseRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PetRepository petRepository;

	@Transactional
	@Override
	public Response purchasePets(PurchasePet purchasePet) {
		Response response = new Response();
		Optional<User> user = userRepository.findById(purchasePet.getUserId());
		Optional<Pet> pet = petRepository.findById(purchasePet.getPetId());

		if (user.isEmpty()) {
			response.setMessage("Please log-in to Purchase pet");

		} else if (user.isPresent() && pet.isEmpty()) {
			response.setMessage("this pet is not available");

		} else {
			if (purchasePet.getQuantity() <= pet.get().getQuantity()) {
				pet.get().setQuantity(pet.get().getQuantity() - purchasePet.getQuantity());
				petRepository.save(pet.get());
				petPurchaseRepository.save(purchasePet);
				response.setMessage("pet purchased successfully");
			} else {
				response.setMessage("choose small quantity pet");
			}
		}
		return response;
	}

	@Override
	public List<PurchasePet> getPurchaseHistory(int id) {
		return petPurchaseRepository.getPurchaseHistoryByUserId(id);
	}

}
