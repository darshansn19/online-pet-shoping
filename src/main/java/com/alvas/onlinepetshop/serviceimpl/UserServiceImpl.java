package com.alvas.onlinepetshop.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.dto.User;
import com.alvas.onlinepetshop.repository.UserRepository;
import com.alvas.onlinepetshop.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository repository;

	public Response userLogin(User user) {

		Response response = new Response();
		User user1 = repository.findByUserName(user.getUserName());

		if (user1 == null) {
			user.setStatus("yes");
			repository.save(user);
			response.setMessage("account created and logged in successfully");
			return response;

		} else if ((user1.getStatus().equals("yes") && (user.getPassword().equals(user1.getPassword()))
				&& (user.getUserName().equals(user1.getUserName())))) {
			response.setMessage("User already Login");
			return response;

		}

		else {
			response.setMessage("Invalid username and password");
			return response;

		}

	}

}
