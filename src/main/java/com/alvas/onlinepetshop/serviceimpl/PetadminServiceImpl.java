package com.alvas.onlinepetshop.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.repository.PetRepository;
import com.alvas.onlinepetshop.service.PetAdminService;

@Service
public class PetadminServiceImpl implements PetAdminService {

	@Autowired
	PetRepository petRepository;

	@Override
	public Response addPet(Pet pet) {
		
		petRepository.save(pet);
		Response response = new Response();
		response.setMessage("pet added successfully");
		return response;
	}

}
