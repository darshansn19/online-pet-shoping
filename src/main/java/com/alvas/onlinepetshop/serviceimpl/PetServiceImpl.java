package com.alvas.onlinepetshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.repository.PetRepository;
import com.alvas.onlinepetshop.service.PetService;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	PetRepository petRepository;

	@Override
	public List<Pet> getAllPets() {
		return petRepository.findAll();
	}

}
