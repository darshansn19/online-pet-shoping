package com.alvas.onlinepetshop.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Response {

	private String message;

}
