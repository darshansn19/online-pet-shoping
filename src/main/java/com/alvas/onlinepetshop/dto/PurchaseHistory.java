package com.alvas.onlinepetshop.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class PurchaseHistory {

	private int petSoldId;

	private int userId;

	private int petId;

	private LocalDate date;

	private int quantity;
	
	private String petName;

}
