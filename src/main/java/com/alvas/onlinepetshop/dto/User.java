package com.alvas.onlinepetshop.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;

@Data
@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;

	@NotEmpty(message = "provide user name")
	private String userName;

	@NotEmpty(message = "provide password")
	private String password;

	@NotEmpty(message = "Email is required")
	@Email(message = "enter valid  email format", regexp = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$")
	@Column(unique = true)
	private String email;

	@ColumnDefault(value = "null")
	private String status;

}
