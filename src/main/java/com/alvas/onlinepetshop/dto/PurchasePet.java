package com.alvas.onlinepetshop.dto;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Data
@Entity
public class PurchasePet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int petSoldId;

	@Min(value = 0, message = "user Id required")
	private int userId;

	@Min(value = 0, message = "pet Id required")
	private int petId;

	@CreationTimestamp
	private LocalDate date;

	@Min(1)
	private int quantity;

}
