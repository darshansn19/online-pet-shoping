package com.alvas.onlinepetshop.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Entity
@Data
public class Pet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int petId;

	@NotEmpty(message = "give pet name")
	private String petName;

	@Min(value = 0, message = "give pet quantity")
	private int quantity;

}
