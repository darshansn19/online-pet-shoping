package com.alvas.onlinepetshop.serviceimpltest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.repository.PetRepository;
import com.alvas.onlinepetshop.serviceimpl.PetadminServiceImpl;

@ExtendWith(SpringExtension.class)
public class PetAdminServiceImplTest {

	@InjectMocks
	PetadminServiceImpl petadminServiceImpl;
	
	@Mock
	PetRepository petRepository;
	
	@Test
	void testAddPet()
	{
		Pet pet=new Pet();
		pet.setPetId(1);
		pet.setPetName("dog");
		pet.setQuantity(4);
		Mockito.when((petRepository.save(pet))).thenReturn(pet);
		Response response=petadminServiceImpl.addPet(pet);
		assertNotNull(response);
		assertEquals("pet added successfully", response.getMessage());
	}

}
