package com.alvas.onlinepetshop.serviceimpltest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.dto.PurchasePet;
import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.dto.User;
import com.alvas.onlinepetshop.repository.PetPurchaseRepository;
import com.alvas.onlinepetshop.repository.PetRepository;
import com.alvas.onlinepetshop.repository.UserRepository;
import com.alvas.onlinepetshop.serviceimpl.PurchasePetServiceImpl;

@ExtendWith(SpringExtension.class)
public class PurchasePetServiceImplTest {

	@InjectMocks
	PurchasePetServiceImpl purchasePetServiceImpl;

	@Mock
	PetPurchaseRepository petPurchaseRepository;

	@Mock
	UserRepository userRepository;

	@Mock
	PetRepository petRepository;

	@Test
	void testpurchasePets() {

		User user = new User();
		user.setUserId(1);
		user.setUserName("darshan");
		user.setPassword("1919");
		user.setEmail("darshu@gmail.com");

		Pet pet1 = new Pet();
		pet1.setPetId(2);
		pet1.setPetName("dog");
		pet1.setQuantity(5);

		PurchasePet purchasePet = new PurchasePet();
		purchasePet.setPetSoldId(1);
		purchasePet.setUserId(1);
		purchasePet.setPetId(2);
		purchasePet.setDate(LocalDate.parse("2023-02-14"));
		purchasePet.setQuantity(1);

		Mockito.when((userRepository.findById(user.getUserId()))).thenReturn(Optional.of(user));
		Mockito.when((petRepository.findById(pet1.getPetId()))).thenReturn(Optional.of(pet1));

		Mockito.when((petRepository.save(pet1))).thenReturn(pet1);
		Mockito.when((petPurchaseRepository.save(purchasePet))).thenReturn(purchasePet);

		Response response = purchasePetServiceImpl.purchasePets(purchasePet);
		assertNotNull(response);
		assertEquals("pet purchased successfully", response.getMessage());

	}

	@Test
	void testpurchasePetForLessPetQuantity() {

		User user = new User();
		user.setUserId(1);
		user.setUserName("darshan");
		user.setPassword("1919");
		user.setEmail("darshu@gmail.com");

		Pet pet1 = new Pet();
		pet1.setPetId(2);
		pet1.setPetName("dog");
		pet1.setQuantity(4);

		PurchasePet purchasePet = new PurchasePet();
		purchasePet.setPetSoldId(1);
		purchasePet.setUserId(1);
		purchasePet.setPetId(2);
		purchasePet.setDate(LocalDate.parse("2023-02-14"));
		purchasePet.setQuantity(6);

		Mockito.when((userRepository.findById(user.getUserId()))).thenReturn(Optional.of(user));
		Mockito.when((petRepository.findById(pet1.getPetId()))).thenReturn(Optional.of(pet1));

		Mockito.when((petRepository.save(pet1))).thenReturn(pet1);
		Mockito.when((petPurchaseRepository.save(purchasePet))).thenReturn(purchasePet);

		Response response = purchasePetServiceImpl.purchasePets(purchasePet);
		assertNotNull(response);
		assertEquals("choose small quantity pet", response.getMessage());

	}

	@Test
	void testgetPurchaseHistory() {
		User user1 = new User();
		user1.setUserId(1);
		user1.setUserName("darshan");
		user1.setPassword("1919");
		user1.setEmail("darshu@gmail.com");

		List<PurchasePet> list = new ArrayList<>();
		PurchasePet purchasePet = new PurchasePet();
		purchasePet.setPetSoldId(1);
		purchasePet.setUserId(1);
		purchasePet.setPetId(2);
		purchasePet.setDate(LocalDate.parse("2023-02-14"));
		purchasePet.setQuantity(2);
		list.add(purchasePet);

		PurchasePet purchasePet1 = new PurchasePet();
		purchasePet1.setPetSoldId(2);
		purchasePet1.setUserId(1);
		purchasePet1.setPetId(3);
		purchasePet1.setDate(LocalDate.parse("2023-02-14"));
		purchasePet1.setQuantity(1);
		list.add(purchasePet1);

		PurchasePet purchasePet2 = new PurchasePet();
		purchasePet2.setPetSoldId(3);
		purchasePet2.setUserId(4);
		purchasePet2.setPetId(2);
		purchasePet2.setDate(LocalDate.parse("2023-02-14"));
		purchasePet2.setQuantity(1);
		

		Mockito.when(petPurchaseRepository.findByUserId(user1.getUserId())).thenReturn(list);
		List<PurchasePet> result = purchasePetServiceImpl.getPurchaseHistory(user1.getUserId());
		assertNotNull(result);
		assertEquals(list, result);
	}

	@Test
	void testpurchasePetsToCheckUserLogin() {

		User user = new User();

		Pet pet1 = new Pet();
		pet1.setPetId(2);
		pet1.setPetName("dog");
		pet1.setQuantity(5);

		PurchasePet purchasePet = new PurchasePet();
		purchasePet.setPetSoldId(1);
		purchasePet.setUserId(1);
		purchasePet.setPetId(2);
		purchasePet.setDate(LocalDate.parse("2023-02-14"));
		purchasePet.setQuantity(1);

		Mockito.when((userRepository.findById(user.getUserId()))).thenReturn(Optional.empty());
		Mockito.when((petRepository.findById(pet1.getPetId()))).thenReturn(Optional.of(pet1));

		Response response = purchasePetServiceImpl.purchasePets(purchasePet);
		assertNotNull(response);
		assertEquals("Please log-in to Purchase pet", response.getMessage());

	}

	@Test
	void testpurchasePetsToCheckPetAvailability() {

		User user = new User();
		user.setUserId(1);
		user.setUserName("darshan");
		user.setPassword("1919");
		user.setEmail("darshu@gmail.com");

		Pet pet1 = new Pet();

		PurchasePet purchasePet = new PurchasePet();
		purchasePet.setPetSoldId(1);
		purchasePet.setUserId(1);
		purchasePet.setPetId(2);
		purchasePet.setDate(LocalDate.parse("2023-02-14"));
		purchasePet.setQuantity(1);

		Mockito.when((userRepository.findById(user.getUserId()))).thenReturn(Optional.of(user));
		Mockito.when((petRepository.findById(pet1.getPetId()))).thenReturn(Optional.empty());

		Response response = purchasePetServiceImpl.purchasePets(purchasePet);
		assertNotNull(response);
		assertEquals("this pet is not available", response.getMessage());

	}

}
