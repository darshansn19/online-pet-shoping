package com.alvas.onlinepetshop.serviceimpltest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.alvas.onlinepetshop.dto.Response;
import com.alvas.onlinepetshop.dto.User;
import com.alvas.onlinepetshop.repository.UserRepository;
import com.alvas.onlinepetshop.serviceimpl.UserServiceImpl;

@ExtendWith(SpringExtension.class)
public class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl serviceImpl;

	@Mock
	UserRepository Repository;

	@Test
	void testUserLogin() {
		User user = new User();
		user.setUserId(1);
		user.setUserName("darshan");
		user.setPassword("1919");
		user.setEmail("darshu@gmail.com");

		User user1 = new User();
		user1.setUserId(1);
		user1.setUserName("darshan");
		user1.setPassword("1919");
		user1.setEmail("darshu@gmail.com");

		Mockito.when((Repository.findByUserName(user.getUserName()))).thenReturn(user1);
		Mockito.when(Repository.save(user)).thenReturn(user);

		Response result = serviceImpl.userLogin(user);
		assertNotNull(user);
		assertEquals("logged in successfully", result.getMessage());

	}
	
	@Test
	void testUserLoginForInValidUserName() {
		User user = new User();
		user.setUserId(1);
		user.setUserName("darshan");
		user.setPassword("1919");
		user.setEmail("darshu@gmail.com");

		User user1 = new User();
		user1.setUserId(1);
		user1.setUserName("ram");
		user1.setPassword("1919");
		user1.setEmail("darshu@gmail.com");

		Mockito.when((Repository.findByUserName(user.getUserName()))).thenReturn(user1);
		Mockito.when(Repository.save(user)).thenReturn(user);

		Response result = serviceImpl.userLogin(user);
		assertNotNull(result);
		assertEquals("Invalid username and password", result.getMessage());
		
		
	}
	
	@Test
	void testUserLoginForInValidPassword() {
		User user = new User();
		user.setUserId(1);
		user.setUserName("darshan");
		user.setPassword("1919");
		user.setEmail("darshu@gmail.com");

		User user1 = new User();
		user1.setUserId(1);
		user1.setUserName("darshan");
		user1.setPassword("1234");
		user1.setEmail("darshu@gmail.com");

		Mockito.when((Repository.findByUserName(user.getUserName()))).thenReturn(user1);
		Mockito.when(Repository.save(user)).thenReturn(user);

		Response result = serviceImpl.userLogin(user);
		assertNotNull(result);
		assertEquals("Invalid username and password", result.getMessage());
				
	}
	
	@Test
	void testUserLoginForInValidUserNameAndassword() {
		User user = new User();
		user.setUserId(1);
		user.setUserName("darshan");
		user.setPassword("1919");
		user.setEmail("darshu@gmail.com");

		User user1 = new User();
		user1.setUserId(2);
		user1.setUserName("ram");
		user1.setPassword("1234");
		user1.setEmail("ram@gmail.com");

		Mockito.when((Repository.findByUserName(user.getUserName()))).thenReturn(user1);
		Mockito.when(Repository.save(user)).thenReturn(user);

		Response result = serviceImpl.userLogin(user);
		assertNotNull(result);
		assertEquals("Invalid username and password", result.getMessage());
		
		
	}
}
