package com.alvas.onlinepetshop.serviceimpltest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.alvas.onlinepetshop.dto.Pet;
import com.alvas.onlinepetshop.repository.PetRepository;
import com.alvas.onlinepetshop.serviceimpl.PetServiceImpl;

@ExtendWith(SpringExtension.class)
public class PetServiceImplTest {
	
	@InjectMocks
	PetServiceImpl petServiceImpl;
	
	@Mock
	PetRepository petRepository;
	
	@Test
	void getAllPetTest()
	{
		List<Pet> list=new ArrayList<>();	
		
		Pet pet1=new Pet();
		pet1.setPetId(1);
		pet1.setPetName("dog");
		pet1.setQuantity(5);
		list.add(pet1);
		
		Pet pet2=new Pet();
		pet2.setPetId(2);
		pet2.setPetName("cat");
		pet2.setQuantity(10);
		list.add(pet2);
		
		Pet pet3=new Pet();
		pet3.setPetId(3);
		pet3.setPetName("parrot");
		pet3.setQuantity(15);
		list.add(pet3);
		
		Pet pet4=new Pet();
		pet4.setPetId(4);
		pet4.setPetName("pigeon");
		pet4.setQuantity(20);
		list.add(pet4);
		
		Mockito.when((petRepository.findAll())).thenReturn(list);
		List<Pet> pets=petServiceImpl.getAllPets();
		assertNotNull(pets);
		assertEquals(list, pets);
	}
	
	

}
